#!/usr/bin/env python3

# Task 1
# 1
class Dog:
    def __init__(self):
        self.age: int = 14
        self.name: str = "Dog"


class Poodle(Dog):
    def __init__(self):
        super().__init__()
        print(f"{self.name} is {self.age} years old")


Poodle()


# 2

class Footballer:
    def __init__(self):
        print("I'm a professional footballer")

    def ball(self) -> None:
        print("playing ball")


class Human(Footballer):
    def __init__(self):
        super().__init__()
        print("I'm a Human")


Human()


# 3

class Stadium:
    def __init__(self):
        pass

    def close(self) -> None:
        print("The stadium is closed")


class Stand(Stadium):
    def __init__(self):
        super().__init__()

    def close(self) -> None:
        print("Stand is closed")

    def forced_close(self) -> None:
        super().close()


Stand().close()


# Task 2
# 1

def user_info():
    name: str = input("Name:")
    surname: str = input("Surname:")
    age: str = input("Old:")

    print("You are {} {}. You're {} years old".format(name, surname, age))


user_info()


# 2

def convert():
    number: str = input("Give number:")
    number: int = int(number)

    print("Number after conversion: {}".format(number))


convert()


# 3

class Flower:
    def __init__(self, flower_name: str, flower_color: str) -> str:
        self.flower_name = flower_name
        self.flower_color = flower_color

        print("Your flower's name is {} and is {}".format(self.flower_name, self.flower_color))


Flower("Tulipan", "red")


# Task 3
# 1

def check_my_name() -> str:
    my_name = input("Your name:")

    if my_name == "Konrad":
        print("Hi Konrad")
    else:
        print("Good morning. Your name: {}".format(my_name))


check_my_name()


# 2
class Rectangle:
    def __init__(self, width: int, height: int):
        self.width = width
        self.height = height

    def area(self) -> int:
        print(self.width * self.height)  # Rectangle area

    def __eq__(self, other):
        return self.area() == other.area()


# 3
class Cube:

    def __init__(self, a: float):
        self.a = a
        self.volume = a * a * a;

    def __eq__(self, other):
        return self.volume == other.volume


# 4
class Electronic:
    def __init__(self, graphic: str, ram: str, processor: str):
        self.graphic = graphic
        self.ram = ram
        self.processor = processor

        print("Electronic")

    def __eq__(self, other):
        return self.graphic == other.graphic and self.ram == other.ram and self.processor == other.processor


class Pc(Electronic):
    def __init__(self, graphic: str, ram: str, processor: str, disk: str):
        self.disk = disk
        super().__init__(graphic, ram, processor, disk)
        print("PC")

    def __eq__(self, other):
        return super().__eq__(other) and self.disk == other.disk


class Phone(Electronic):
    def __init__(self, graphic: str, ram: str, processor: str, size: str):
        super().__init__(graphic, ram, processor)
        self.size = size
        print("Phone")

    def __eq__(self, other):
        return super().__eq__(other) and self.size == other.size


class Xiaomi(Phone):
    def __init__(self, ram: str, size: str):
        super().__init__(ram, size)
        print("Xiaomi")


class Samsung(Phone):
    def __init__(self, size: str):
        super().__init__(size)
        print("Samsung")