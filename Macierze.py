import numpy

X = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

Y = [
    [7, 4, 30],
    [2, 3, 60],
    [74, 78, 59]
]

s = 2  # scalar-vector value

print("\n")
print("Adding matrix")

result = [[X[i][j] + Y[i][j] for j in range(len(X[0]))] for i in range(len(X))]

for r in result:
    print(r)

print("\n")
print("Transpose matrix")

result1 = [[X[j][i] for j in range(len(X))] for i in range(len(X[0]))]

for r in result:
    print(r)

print("\n")
print("Multiplying matrix")

# zagnieżdżone przetwarzanie list, aby iterować po każdym elemencie macierzy
# wbudowana funkcja zip () i rozpakowywanie listy argumentów za pomocą operatora *

result = [[sum(a * b for a, b in zip(X_row, Y_col)) for Y_col in zip(*Y)] for X_row in X]

for r in result:
    print(r)

print("\n")
print("Scalar multiplication matrix")

result = [[X[i][j] * s for j in range(len(X[0]))] for i in range(len(X))]

for r in result:
    print(r)

print("\n")
print("det(X)")
print(numpy.linalg.det(X))  # wyznacznik

print("\n")
print("det(Y)")
print(numpy.linalg.det(Y))
